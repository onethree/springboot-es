package com.ithzk.es.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.ithzk.es.dao")
public class MybatisConfig {

}
