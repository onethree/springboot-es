package com.ithzk.es;

import com.alibaba.fastjson.JSON;
import com.ithzk.es.dao.NBAPlayerMapper;
import com.ithzk.es.entity.NBAPlayer;
import com.ithzk.es.service.NBAPlayerService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
class SpringbootEsApplicationTests {

	@Autowired
	private NBAPlayerService nbaPlayerService;

	@Autowired
	private NBAPlayerMapper nbaPlayerMapper;

	@Test
	void addPlayer() throws IOException {
		NBAPlayer nbaPlayer = new NBAPlayer().setId(1).setDisplayNameEn("Tracy McGrady").setJerseyNo("1");
		nbaPlayerService.addPlayer(nbaPlayer, String.valueOf(nbaPlayer.getId()));
	}

	@Test
	void updatePlayer() throws IOException {
		NBAPlayer nbaPlayer = new NBAPlayer().setId(1).setDisplayNameEn("Lebron James").setJerseyNo("6");
		nbaPlayerService.updatePlayer(nbaPlayer, String.valueOf(nbaPlayer.getId()));
	}

	@Test
	void deletePlayer() throws IOException {
		nbaPlayerService.deletePlayer("1");
	}

	@Test
	void getPlayer() throws IOException {
		nbaPlayerService.getPlayer("1");
	}

	@Test
	void importAll() throws IOException {
		nbaPlayerService.importAll();
	}

}
