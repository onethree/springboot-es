package com.ithzk.es.dao;

import com.ithzk.es.entity.NBAPlayer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface NBAPlayerMapper {

    @Select("select * from nba_player")
    public List<NBAPlayer> selectAll();
}
